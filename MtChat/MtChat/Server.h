#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <iostream>
#include <queue>
class Server
{
public:
	Server();
	~Server();

	std::string readFromFile(std::string path);

	void serve(int port);
private:

	void accept();
	void clientHandler(SOCKET clientSocket);
	SOCKET _serverSocket;
};

