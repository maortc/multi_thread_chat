#include "Server.h"
#include "Helper.h"
#include <exception>
#include <iostream>
#include <cstdlib>
#include <string>
#include <queue>
#include <thread>
#include <mutex>
#include <chrono>
#include <fstream>
#include <Windows.h>
#include <condition_variable>

// Vectors
std::vector<std::thread > thread_pool;
std::vector<std::string> users;


// Mutex
std::mutex mtx;
std::mutex _mMessages;


using std::cout;
using std::endl;

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET,  SOCK_STREAM,  IPPROTO_TCP); 

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{
	
	struct sockaddr_in sa = { 0 };
	
	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");
	
	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
    
    thread_pool.push_back(std::thread(&Server::clientHandler, this, std::ref(client_socket)));
    thread_pool[thread_pool.size()-1].detach();
    std::this_thread::sleep_for(std::chrono::seconds(2));

	// the function that handle the conversation with the client
	
}


void Server::clientHandler(SOCKET clientSocket)
{
    std::string username;
        try
        {
        
            int userMessageType = 0;
            int usernameLength = 0;
            int i = 0;
            std::string loginReply;
            std::string allNames;
        
            while (userMessageType != MT_CLIENT_EXIT)
            {
                allNames = "";
                userMessageType = Helper::getMessageTypeCode(clientSocket);
            
                switch (userMessageType)
                {
                case MT_CLIENT_LOG_IN:
                {   
                
                    std::lock_guard<std::mutex> lock(mtx);
                    usernameLength = Helper::getIntPartFromSocket(clientSocket, 2);
                    username = Helper::getStringPartFromSocket(clientSocket, usernameLength);
                
                    for (i = 0; i < users.size(); i++)
                    {
            
                        allNames += users[i];
                        allNames += "&";
                    }           
                    allNames += username;

                    Helper::send_update_message_to_client(clientSocket, "", "", allNames);
                    users.push_back(username);

                    break;
                }

                case MT_CLIENT_UPDATE:
                {
                    std::lock_guard<std::mutex> lock(mtx);

                    allNames = "";
                    std::string strTotalUserLen = "";
                    std::string name_to_send;
                    std::string data;
                    std::string fileData;
                    int nameLength;
                    int dataLength;


                    for (i = 0; i < users.size(); i++)
                    {
                        allNames += users[i];
                        allNames += "&";

                    }
                    allNames.erase(allNames.size() - 1, allNames.size());
                
                    nameLength = Helper::getIntPartFromSocket(clientSocket, 2);
                    name_to_send = Helper::getStringPartFromSocket(clientSocket, nameLength);
               
              
                    dataLength = Helper::getIntPartFromSocket(clientSocket, 5);
                    data = Helper::getStringPartFromSocket(clientSocket, dataLength);
                

                    std::string name_list[2] = { username, name_to_send };
                    std::sort(std::begin(name_list), std::end(name_list));
                    std::string path = name_list[0] + "&" + name_list[1] + ".txt";
                

                    if (data.size() > 0)
                    {
                        data = "&MAGSH_MESSAGE&&Author&" + username + "&DATA&" + data;
                        std::ofstream outfile;
                        outfile.open(path, std::ios_base::app);
                        outfile << data;
                   
                        fileData = readFromFile(path);
                        Helper::send_update_message_to_client(clientSocket, fileData, name_to_send, allNames);
                        outfile.close();
                    }
                    else
                    {
                        fileData = readFromFile(path);
                        Helper::send_update_message_to_client(clientSocket, fileData, name_to_send, allNames);
                    }
                    break;
                    }              
                }
            }
       
            closesocket(clientSocket);
        }

    catch (const std::exception& e)
    {
        for (std::vector<std::string>::iterator iter = users.begin(); iter != users.end(); ++iter)
        {
            if (*iter == username)
            {
                users.erase(iter);
                return;
            }
        }  
    }
}



std::string Server::readFromFile(std::string path)
{
    std::ifstream dataF(path, std::ios::in);
    std::string line;
    std::string msg = "";

    while (std::getline(dataF, line))
    {
        std::unique_lock<std::mutex> lck(_mMessages);
        msg += line;
        lck.unlock();
        
    }
    dataF.close();
    return msg;
}

